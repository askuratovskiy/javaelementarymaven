package je.Lesson17;

import java.io.*;

public class CwSerialMain {

    public static void main(String[] args) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("temp.out"))) {
            CwSerialObj ts = new CwSerialObj();
            oos.writeObject(ts);
            oos.flush();
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try(ObjectInputStream oin = new ObjectInputStream(new FileInputStream("temp.out"))) {
            CwSerialObj ts = (CwSerialObj) oin.readObject();
            System.out.println(ts.x);
            System.out.println(ts.c);
            System.out.println(ts.m);
            System.out.println(ts.p);
        }catch (IOException | ClassNotFoundException ex){
            //
        }


    }
}
