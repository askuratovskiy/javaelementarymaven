package je.lesson02;

import java.util.Random;

public class hwMatrix08 {
    public static void main(String[] args) {
        task01();
        task02();
    }

    //Найти минимальные и максимальные элементы в матрице
    private static void task01(){
        int row = 5;
        int column = 8;
        int [] [] arr = new int [row][column];
        Random random = new Random();
        for(int i=0; i<row; i++){
            for (int b=0; b<column; b++){
                arr[i][b] = random.nextInt(21)-10;
            }
        }
        //Matrix print
        for(int i=0; i<row; i++){
            for (int b=0; b<column; b++){
                System.out.print(arr[i][b]+" ");
            }
            System.out.println();
        }
        //Search min
        int tmpMinRow = 0;
        int tmpMinColumn = 0;
        for(int i=0; i<row; i++){
            for (int b=0; b<column; b++){
                if(arr[tmpMinRow][tmpMinColumn] > arr[i][b]){
                    tmpMinRow = i;
                    tmpMinColumn = b;
                }
            }
        }
        //Search max
        int tmpMaxRow = 0;
        int tmpMaxColumn = 0;
        for(int i=0; i<row; i++){
            for (int b=0; b<column; b++){
                if(arr[tmpMaxRow][tmpMaxColumn] < arr[i][b]){
                    tmpMaxRow = i;
                    tmpMaxColumn = b;
                }
            }
        }
        System.out.println("Min "+arr[tmpMinRow][tmpMinColumn]+" arr["+tmpMinRow+"]["+tmpMinColumn+"]");
        System.out.println("Max "+arr[tmpMaxRow][tmpMaxColumn]+" arr["+tmpMaxRow+"]["+tmpMaxColumn+"]");


    }

    //Вывести на экран строку,сумма элементов которая максимальна
    private static void task02(){
        int row = 5;
        int column = 8;
        int maxRow = 0;
        int maxSum = 0;
        int [] [] arr = new int [row][column];
        Random random = new Random();
        for(int i=0; i<row; i++){
            for (int b=0; b<column; b++){
                arr[i][b] = random.nextInt(21)-10;
            }
        }
        for(int i=0; i<row; i++){
            int tmpSum =0;
            for (int b=0; b<column; b++){
                tmpSum +=arr[i][b];
            }
            if (tmpSum > maxSum){
                maxSum = tmpSum;
                maxRow = i;
            }
        }
        System.out.print("Row "+maxRow+": ");
        for (int b=0; b<column; b++){
            System.out.print(arr[maxRow][b]+" ");
        }
    }
}


