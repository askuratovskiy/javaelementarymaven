package je.lesson02;


public class hwArray05 extends arrayMethod {

    public static void main(String[] args) {
        task01();
        task02();
        task03();
        task04();
        task05();
        task06();
    }

    //Создайте массив из всех чётных чисел от 2 до 20 и выведите элементы массива на экран сначала в строку,
    // отделяя один элемент от другого пробелом, а затем в столбик (отделяя один элемент от другого началом новой строки).
    // Перед созданием массива подумайте, какого он будет размера
    private static void task01(){
        int [] arr = gererateRandomArray(4,2,20);
        System.out.println(printArr(arr));
        for (int i : arr){System.out.println(i);}
    }

    //Создайте массив из всех нечётных чисел от 1 до 99, выведите его на экран в строку,
    // а затем этот же массив выведите на экран тоже в строку,
    // но в обратном порядке (99 97 95 93 … 7 5 3 1).
    private static void task02(){
        int [] arr = new int[100/2];
        int arrIndex = 0;
        for (int i=1; i<100; i++){
            if(i%2!=0){
                arr[arrIndex] = i;
                arrIndex++;            }
        }
        System.out.println(printArr(arr)+"\n");
        System.out.println(printArrRevers(arr));
    }

    //Создайте массив из 15 случайных целых чисел из отрезка [0;9]. Выведите массив на экран.
    // Подсчитайте сколько в массиве чётных элементов и выведете это количество на экран на отдельной строке.
    private static void task03(){
        int [] arr = gererateRandomArray(15,0,9);
        int countPositiveNum=0;
        for (int i : arr){
            if(i%2==0 && i!=0){
                countPositiveNum++;
            }
        }
        System.out.println(printArr(arr));
        System.out.println("чётных элементов = " + countPositiveNum);
    }

    //Создайте массив из 8 случайных целых чисел из отрезка [1;10]. Выведите массив на экран в строку.
    // Замените каждый элемент с нечетным индексом на ноль. Снова выведете массив на экран на отдельной строке.
    private static void task04() {
        int[] arr = gererateRandomArray(8, 1, 10);
        System.out.println(printArr(arr));
        for (int i =0; i<arr.length; i++) {
            if (i % 2 != 0) { arr[i] = 0; }
        }
        System.out.println(printArr(arr));
    }

    // Создайте 2 массива из 5 случайных целых чисел из отрезка [0;5] каждый, выведите массивы на экран в двух отдельных строках.
    // Посчитайте среднее арифметическое элементов каждого массива и сообщите,
    // для какого из массивов это значение оказалось больше (либо сообщите, что их средние арифметические равны).
    private static void task05(){
        int[] arrFirst = gererateRandomArray(5, 0, 5);
        int[] arrSecond = gererateRandomArray(5, 0, 5);
        int sumFirst=0;
        int sumSecond=0;
        int averageNumFirst;
        int averageNumSecond;

        for (int i=0; i<arrFirst.length-1; i++){
            sumFirst+=arrFirst[i];
            sumSecond+=arrSecond[i];
        }
        averageNumFirst = sumFirst/arrFirst.length;
        averageNumSecond = sumSecond/arrSecond.length;

        System.out.println(printArr(arrFirst)+" average = "+ averageNumFirst);
        System.out.println(printArr(arrSecond)+" average = "+ averageNumSecond);
        if (averageNumFirst == averageNumSecond){
            System.out.println("equals");
        }else
            System.out.println(averageNumFirst > averageNumSecond ? "first large second" : "second large first");
    }

    //Создайте массив из 4 случайных целых чисел из отрезка [10;99], выведите его на экран в строку.
    //Определить и вывести на экран сообщение о том, является ли массив строго возрастающей последовательностью.
    private static void task06(){
        int[] arr = gererateRandomArray(4, 10, 99);
        System.out.println(printArr(arr));
        int flag=0;
        for (int i=0; i<arr.length-1; i++){
            if(arr[i]<arr[i+1]){
                flag=1;
            }
        }
        System.out.println(flag==0 ? "строго возрастающей" : "строго не по возрастающей");
    }
}
