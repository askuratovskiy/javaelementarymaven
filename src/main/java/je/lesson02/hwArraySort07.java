package je.lesson02;

import java.util.Arrays;
import java.util.Scanner;

public class hwArraySort07 extends arrayMethod {

    public static void main(String[] args) {
        task01();
        task02();
        task03();
    }


    //  1.arr[10] range [0..100]
    //  2.sort on the last digit
    private static void task01(){
        int [] arr = gererateRandomArray(10,0,100);
        System.out.println(printArr(arr));
        for(int i = arr.length-1 ; i > 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j]%10 > arr[j + 1]%10) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        System.out.println(printArr(arr));
    }

    //1. arr[10] range [0..100]
    //2. Отсортировать первую половину по возрастанию, а вторую по убыванию.
    private static void task02(){
        int [] arr = gererateRandomArray(10,0,100);
        System.out.println(printArr(arr));
        for(int i = arr.length/2-1 ; i > 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        for(int i = arr.length-1 ; i > 0 ; i--) {
            for (int j = arr.length/2; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        System.out.println(printArr(arr));
    }

    //Написать программу, которая сортирует массив ПО УБЫВАНИЮ и ищет в нем элемент, равный X (это число вводится с клавиатуры).
    private static void task03() {
        int[] arr = gererateRandomArray(20, 0, 100);
        //Sort arr
        Arrays.sort(arr);
        //Revers arr
        for (int i = 0; i < arr.length / 2; i++) {
            int tmp = arr[i];
            arr[i] = arr[(arr.length - 1) - i];
            arr[(arr.length - 1) - i] = tmp;
        }

        System.out.println("Enter the number what do you want search");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        if (arr[arr.length / 2] >= x) {
            for (int i = arr.length / 2; i < arr.length; i++) {
                if (arr[i] == x) {
                    System.out.println(x + " found");
                    break;
                }
            }
        } else {
            for (int i = 0; i < arr.length / 2; i++) {
                if (arr[i] == x) {
                    System.out.println(x + " founded");
                    break;
                }
            }
        }
    }
}
