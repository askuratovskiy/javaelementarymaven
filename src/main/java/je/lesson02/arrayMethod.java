package je.lesson02;

import java.util.Random;

public class arrayMethod {

    public static String printArr(int[] arr) {
        String arrToString = "";
        for (int i : arr) {
            arrToString = arrToString + (i + " ");
        }
        return arrToString;
    }

    public static String printArrRevers(int[] arr) {
        String arrToString = "";
        for (int i = arr.length-1; i >=0; i--) {
            arrToString = arrToString + (arr[i] + " ");
        }
        return arrToString;
    }

    //Range -N .. +N, +N .. +N
    //End value must be possitive
    public static int[] gererateRandomArray(int arraySize, int randomRangeStat, int randomRangeEnd) {
        int[] arr = new int[arraySize];
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            if (randomRangeStat < 0) {
                arr[i] = (random.nextInt(((randomRangeStat) - (randomRangeStat * 2) + randomRangeEnd) + 1)) + randomRangeStat;
            } else if (randomRangeStat == 0) {
                arr[i] = (random.nextInt(randomRangeEnd + 1));
            } else if (randomRangeStat > 0) {
                arr[i] = (random.nextInt((randomRangeEnd - randomRangeStat + 1)) + randomRangeStat);
            }
        }
        return arr;
    }

}
