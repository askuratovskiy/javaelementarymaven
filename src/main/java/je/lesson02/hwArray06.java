package je.lesson02;

public class hwArray06 extends arrayMethod{

    public static void main(String[] args) {
        task01();
    }

    //Заполнить массив случайными числами и выделить в другой массив все числа, которые встречаются более одного раза.
    private static void task01() {
        //Генерируем массив
        int[] arr = gererateRandomArray(50, -100, 55);
        //Создаем массив для повторяющихся чисел
        int[] twice = new int[arr.length/2];
        //Счетчик для массия twice
        int countTwice = -1;
        // Пробегаемся по массиву arr c хвоста в начало
        for (int a = arr.length-1; a >=0; a--) {
            // Бежим с начала до 'a'
            for (int b = 0; b<a; b++){
                // Сравниваем значения на совпадение
                if(arr[a] == arr[b]){
                    //Флаг совпадения
                    int flag = 1;
                    // Проверка счетчика
                    if(countTwice >=0) {
                        // Пробегаемся по массиву twice
                        for (int c = 0; c <= countTwice; c++) {
                            // Проверяем есть ли в массиве дубль число
                            if (arr[a] == twice[c]) { flag = 0; }
                        }
                    }
                    // Если дубль числа нет в twice - добавляем в массив.
                    if (flag==1){
                        countTwice++;
                        twice[countTwice] = arr[a];
                    }
                }
            }

        }
        System.out.println("Массив чисел arr[]");
        System.out.println(printArr(arr));
        System.out.println("Массив чисел twice[] которой содержит дублирующиеся числа в массиве arr[]");
        for (int i=0; i<=countTwice; i++){
            System.out.print(twice[i] + " ");
        }
    }
}








