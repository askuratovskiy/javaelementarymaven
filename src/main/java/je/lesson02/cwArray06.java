package je.lesson02;

import java.util.Random;
import java.util.Scanner;

public class cwArray06 {
    public static void main(String[] args) {
        //task01();
        //task02();
        //task03();
        task04();
    }

    private static void task01() {
        Scanner scanner = new Scanner(System.in);
        int [] arr = new int[5];
        int i = 0;
        int sum = 0;
        while (i < 5) {
            System.out.println("Enter number");
            arr[i] = scanner.nextInt();
            sum = sum + arr[i];
            i++;
        }
        System.out.println("Среднее = "+ sum/(arr.length-1));
    }

    private static void task02(){
        int [] arr = new int[10];
        Random random = new Random ();
        for (int i =0 ; i<arr.length-1; i++){
            arr[i] = random.nextInt(21)-10;
        }
        for (int a = 0; a<arr.length-1; a++)
            System.out.print(arr[a]);



    }

    //Заполнить массив из 10 элементов случайными числами в интервале [-10..10]
    // и выполнить инверсию отдельно для 1-ой и 2-ой половин массива.
    private static void task03(){
        int [] arr = new int[10];
        Random random = new Random ();
        for (int i =0 ; i<arr.length; i++){
            arr[i] = random.nextInt(21)-10;
        }

        System.out.println("First");
        for (int a = 0; a<arr.length; a++){
            System.out.print(arr[a]+" ");
        }
        System.out.println();

        for (int i=0; i<(arr.length/2)/2; i++){
            int tmp = arr[i];
            arr[i] = arr[(arr.length/2-1)-i];
            arr[(arr.length/2-1)-i] = tmp;

            tmp = arr[(arr.length-1)-i];
            arr[(arr.length-1)-i] = arr[(arr.length/2)+i];
            arr[(arr.length/2)+i] = tmp;
        }
        System.out.println("Second");
        for (int a = 0; a<arr.length; a++){
            System.out.print(arr[a]+" ");
        }

    }

    //Заполнить массив из 10 элементов случайными числами в интервале [-10..10] и выполнить циклический сдвиг ВЛЕВО.
    private static void task04(){
        //add array
        int [] arr = new int[10];
        Random random = new Random ();
        for (int i =0 ; i<arr.length-1; i++){
            arr[i] = (random.nextInt(21))-10;
        }

        //Print before change
        System.out.println("Befor");
        for (int a = 0; a<arr.length-1; a++)
            System.out.print(arr[a]+" ");
        System.out.println("");

        //Change arr
        int first = arr[0];
        for (int i = 0; i<arr.length-1; i++){
            arr[i] = arr[i+1];
        }
        arr[arr.length-1] = first;

        //Print after change
        System.out.println("After");
        for (int a = 0; a<arr.length-1; a++)
            System.out.print(arr[a]+" ");
    }

}
