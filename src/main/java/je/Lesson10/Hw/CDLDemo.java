package je.Lesson10.Hw;

import java.util.concurrent.CountDownLatch;

public class CDLDemo {
    public static void main(String[] args) {
        CountDownLatch cd1 = new CountDownLatch(5);

        System.out.println("Starting main thread");

        new MyThread(cd1);

        try {
            cd1.await();
        }catch (InterruptedException e){
            System.out.println(e);
        }
        System.out.println("Stopping the main thread");
    }

    static class MyThread implements Runnable{
        CountDownLatch latch;
        MyThread(CountDownLatch c){
            this.latch =c;
            new Thread(this).start();
        }

        @Override
        public void run() {
            for (int i = 0; i < 3; i++){
                System.out.println(i);
                System.out.println(latch);
                latch.countDown();
            }
        }
    }
}
