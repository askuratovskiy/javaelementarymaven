package je.Lesson14;

public abstract class HwAbstractArray<T extends Number> {
    public T [] sort(T [] arr) {
        Class c = arr.getClass();
        for (int i = arr.length - 1; i >= 0; i--) {
            T tmp = arr[i];
            for (int k = 0; k < i; k++) {

                if (c == Integer[].class) {
                    if ((Integer) arr[k] > (Integer) tmp) {
                        arr[i] = arr[k];
                        arr[k] = tmp;
                        tmp = arr[i];
                    }
                    if (c == Double[].class) {
                        if ((Double) arr[k] > (Double) tmp) {
                            arr[i] = arr[k];
                            arr[k] = tmp;
                            tmp = arr[i];
                        }
                        if (c == Float[].class) {
                            if ((Float) arr[k] > (Float) tmp) {
                                arr[i] = arr[k];
                                arr[k] = tmp;
                                tmp = arr[i];
                            }

                        }
                    }
                }
            }
        }
        return arr;
    }
}
