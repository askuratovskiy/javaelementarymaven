package je.Patterns.Factory;

public abstract class AbstractWriter {
    public abstract void write(Object context);
}
