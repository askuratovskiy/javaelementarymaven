package je.Patterns.Factory;

public class Writers {

    public static class ConcreteFileWriter extends AbstractWriter {
        public void write (Object context) {
            // method body
        }
    }

    public  static class ConcreteXmlWriter extends AbstractWriter {
        public void write (Object context) {
            // method body
        }
    }
}
