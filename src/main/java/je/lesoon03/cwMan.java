package je.lesoon03;

public class cwMan {

    cwMan(){ System.out.println("Man constructor"); }

    static int a = 3;
    static  int b;
    static void method(int x){
        System.out.println("x = "+x);
        System.out.println("a = "+a);
        System.out.println("b = "+b);
    }

    { System.out.println("block init"); }

    static {
        System.out.println("static init");
        b = a * 4;
    }

    public static void main(String[] args) {
        method(42);
        cwMan man = new cwMan();
    }

}
