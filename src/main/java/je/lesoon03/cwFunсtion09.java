package je.lesoon03;

import java.util.Scanner;

public class cwFunсtion09 {
    public static void main(String[] args) {
        task01();
        task02();
        task03();
        task04();
    }

    //Составить функцию, которая определяем сумму чисел от 1 до N
    private static void task01(){
        System.out.println("Enter the num");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int sum=0;
        for (int i =1; i<=num; i++){
            sum+=num;
        }
        System.out.println(sum);
    }

    //Составить функцию, которая рисует линию из символа symb
    private static void task02(){
        System.out.println("Enter the num");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        for(int i=1; i<=num; i++){
            System.out.print("%");
        }
    }

    //Составить функцию, которая определяет, верно ли, что сумма его цифр - четное число
    private static void task03(){
        System.out.println("Enter the num");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int sum = 0;
        while (num!=0){
            sum= sum + num%10;
            num = num/10;
        }
        System.out.println(sum%2==0?"Yes": "No");
    }

    //Составить функцию, которая определяет, верно ли, что в заданом числе все цифры стоят по возрастонию
    private static void task04() {
        System.out.println("Enter the num");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        while (num != 0) {
            int tmpLast = num % 10;
            num = num / 10;
            if (tmpLast < num % 10) {
                break;
            }
            System.out.println(num == 0 ? "All good" : "Error");
        }
    }


}
