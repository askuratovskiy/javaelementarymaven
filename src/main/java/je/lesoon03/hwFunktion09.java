package je.lesoon03;

import java.util.Scanner;

public class hwFunktion09 {
    public static void main(String[] args) {
        //task01();
        task02();

    }

    //Составить функцию, которая спросит число и степень в которую необходимо возвести число
    private static void task01(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the num");
        int num = scanner.nextInt();
        System.out.println("Enter the exponent");
        int exponet = scanner.nextInt();
        int result = num;
        for(int i=1; i < exponet; i++){
            result = result* num;
        }
        System.out.println(result);
    }

    //Составить функцию, которая определяет наибольшый общий делитель двух натуральных чисел
    private static void task02(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first num");
        int numFirst = scanner.nextInt();
        System.out.println("Enter the second num");
        int numSecond = scanner.nextInt();
        int divider = 1;
        int dividerMax = 0;
        while (numFirst/divider!=0 && numSecond/divider!=0){
            if(numFirst%divider==0 && numSecond%divider==0){
                dividerMax = divider;
            }
            divider++;
        }
        System.out.println("НОД("+numFirst+","+numSecond+") = "+dividerMax);
    }
}
