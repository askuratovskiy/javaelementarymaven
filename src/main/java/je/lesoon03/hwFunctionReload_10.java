package je.lesoon03;

import java.util.Random;

public class hwFunctionReload_10 {
    public static void main(String[] args) {
//        task01
//        int arrInt [] = {1,2,3,4,5,6,7};
//        float arrFloat [] = {1.1f,2.2f,3.3f,4.4f,5.5f,6.6f,7.7f};
//        System.out.println(task01.averageArray(arrInt));
//        System.out.println(task01.averageArray(arrFloat));

        //task02
//        int intFistr = 2;
//        int intSecond = 2;
//        float floarFisrt = 2.2f;
//        float floarSecond = 2.2f;
//        System.out.println(task02.squareNumber(intFistr, intSecond));
//        System.out.println(task02.squareNumber(floarFisrt, floarSecond));

        //task03
//        int arr [] = new int[10];
//        task03.randomArray(arr);

        A a = new B();
        a.callme();

    }

    //Написать программу с функциями для подсчета среднего арифметичесикого массива для различных типов данных (int,float)
    static class task01{
        static int averageArray(int[] arr){
            int sum = 0;
            for (int i : arr){ sum+=i; }
            return sum/arr.length;
        }
        static float averageArray(float [] arr){
            float sum = 0;
            for (float i : arr){ sum+=i; }
            return sum/arr.length;
        }
    }

    //Написать программу с функциями для подсчета суммы квадратов двух чисел для различных типов данных (int,float)
    static class task02{
        static int squareNumber(int numFirst, int numSecond){
            return (numFirst * numFirst + numSecond * numSecond);
        }
        static float squareNumber(float numFirst, float numSecond){
            return (numFirst * numFirst + numSecond * numSecond);
        }
    }

    //Заполнить массив из 10 элементов случаными не повторяющимися числами
    static class task03{
        static void randomArray(int [] arr){
            int cursor = -1;
            //Filling array
            recursion(arr, cursor);
            //Print array
            for (int i : arr){
                System.out.print(i+" ");
            }
        }
        static void recursion(int[]arr, int cursor){
            if(cursor < arr.length-1){
                int randomNum;
                boolean flag;
                do{
                    Random random = new Random();
                    randomNum = random.nextInt(10);
                    flag = false;
                    for (int d=0; d<=cursor; d++){
                        if(arr[d] == randomNum){ flag = true; }
                    }
                }while (flag == true);
                cursor++;
                arr[cursor] = randomNum;
                recursion(arr, cursor);
                }
            }
    }
    static class A {
        void callme() {
            System.out.println("Inside A's callme method");
        }
    }
    static class B extends A {
        void callme() {
            System.out.println("Inside B's callme method");
        }
    }

}
