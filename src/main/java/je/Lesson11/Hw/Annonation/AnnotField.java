package je.Lesson11.Hw.Annonation;


import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface AnnotField {
    String author();
    String description() default "false";
}
