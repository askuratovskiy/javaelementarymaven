package je.Lesson11.Hw.Annonation;


import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AnnotTypeVersion {
    String authon();
    String date() default "year-month-date";
    String [] comments();
}
