package je.Lesson11.Hw.Annonation;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.lang.annotation.Annotation;

/*
Реализовать несколько своих аннотаций (для класса, метода и поля) с несколькими параметрами + параметр по умолчанию.
Реализовать класс в котором использовать выше реализованные аннотации.
С помощью отражения получить информацию об аннотациях в данном классе.
*/

@AnnotTypeVersion(authon = "Alex", date = "2018-05-19", comments = {"clean", "install", "package"} )
public class AnnotClass {

    @AnnotField(author = "Alex", description = "Age")
    private int age;
    @AnnotField(author = "Alex", description = "Name")
    private String name;
    private String loocation;
    private Date currentDate;

    AnnotClass(String name, int age, String location, Date currentDate){
        this.name = name;
        this.age = age;
        this.loocation = location;
        this.currentDate = currentDate;
    }

    @AnnotMetod(author = "Alex", description = "getAge()")
    public int getAge() {
        return age;
    }
    @AnnotMetod(author = "Alex", description = "setAge()")
    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoocation() {
        return loocation;
    }

    public void setLoocation(String loocation) {
        this.loocation = loocation;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public static void main(String[] args) {

        //Get Class annotation
        Class cc = AnnotClass.class;
        Annotation [] ann = cc.getDeclaredAnnotations();
        Method [] methods = cc.getDeclaredMethods();
        for(Annotation annotation : ann){
            System.out.println(annotation);
        }

        //Get Fields annotation
        for (Field field : cc.getDeclaredFields()){
            Annotation [] annField = field.getDeclaredAnnotations();
            for (Annotation annotation : annField){
                System.out.println(annotation);
            }
        }

        //Get Methods annotation
        for(Method method : cc.getDeclaredMethods()){
            Annotation [] annMethod = method.getDeclaredAnnotations();
            for (Annotation annotation : annMethod){
                System.out.println(annotation);
            }
        }


    }

}
