package je.Lesson11.Hw.Annonation;


import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AnnotMetod {
    String author();
    String description();
}
