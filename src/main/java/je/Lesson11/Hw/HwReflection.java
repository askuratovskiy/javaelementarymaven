package je.Lesson11.Hw;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class HwReflection {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        HashMap<Integer, String> hashMap = new HashMap<>();

        //1. Создать через отражение экземпляр класса HashMap
        System.out.println("--------- Part 1 ---------");
        Class clashHashMap = HashMap.class;

        //1.1. Добавить несколько элемента одного типа
        for (int i=0; i<10; i++){
            Method methodPut = clashHashMap.getDeclaredMethod("put", Object.class, Object.class);
            Object [] put = new Object[]{new Integer(i), new String("Vasya"+i)};
            Object tmp = methodPut.invoke(hashMap, put);
        }

        //1.2. Получить элементы и вывести на экран
        for (int i=0; i<10; i++){
            Method methodGet = clashHashMap.getDeclaredMethod("get", Object.class);
            System.out.println("Key = " + i +" Value = " +methodGet.invoke(hashMap, i));
        }

        //1.3. Проверить что экземпляр является тем, который мы ожидаем
        System.out.println("Проверить что экземпляр является тем, который мы ожидаем "+clashHashMap.isInstance(HashMap.class));

        //1.4. Вывести на экран родительский класс класса HashMap
        System.out.println("Вывести на экран родительский класс класса HashMap "+clashHashMap.getSuperclass());

        //1.5 Вывести реализуемые интерфейсы классом HashMap
        System.out.println("Вывести реализуемые интерфейсы классом HashMap");
        Class [] interfaces = clashHashMap.getInterfaces();
        for (Class interf : interfaces){
            System.out.println(interf);
        }

        //2. Вывести на экран все открытые методы и поля класса ArrayLis
        System.out.println("--------- Part 2 ---------");
        Class classArrayList = ArrayList.class;
        Method [] publicMethods = classArrayList.getMethods();
        System.out.println("--------- publicMethods list ---------");
        for (Method pubMeth : publicMethods){
            System.out.println(pubMeth);
        }

        Field [] publicFields = classArrayList.getFields();
        System.out.println("--------- publicFields list ---------");
        for (Field pubFields : publicFields){
            System.out.println(pubFields);
        }

        //2.1. Вывести на экран все скрытые данные класса ArrayList
        Method [] privateMethods = classArrayList.getDeclaredMethods();
        System.out.println("--------- privateMethods list ---------");
        for (Method priMeth : privateMethods){
            System.out.println(priMeth);
        }

        Field [] privateFields = classArrayList.getDeclaredFields();
        System.out.println("--------- privateFields list ---------");
        for (Field priFields : privateFields){
            System.out.println(priFields);
        }

        //3.1. Динамически изменить значение приватного поля.
        System.out.println("--------- Part 3 ---------");
        MyClass myClass = new MyClass();
        Class classMyClass = myClass.getClass();
        //Change public var
        Field pubIntField = classMyClass.getField("pubInt");
        pubIntField.setInt(myClass, 10);
        //Change private var
        Field priIntField = classMyClass.getDeclaredField("priInt");
        priIntField.setAccessible(true);
        priIntField.setInt(myClass, 11);

        //3.2. Все вывести на экран
        System.out.println("pubInt = "+pubIntField.getInt(myClass));
        System.out.println("priInt = "+priIntField.getInt(myClass));

        //4. Получить ошибку ClassCastException
        

    }
//3. Создать свой класс с приватным полем и публичным
   static class MyClass{
        public int pubInt = 0;
        private int priInt = 1;
    }

}



