package je.Lesson08.Cw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class CwCollection {

    static class Collect{
        public static void task01(){
            List<String> underTest = new ArrayList<>();
            List test = underTest;
            try {
                test.add("1");
                test.add("2");
                test.add("3");
                test.add(new Integer(4));
                System.err.println(underTest);
            } catch (ClassCastException ex) {
                System.err.println("Cast Exceprtion while first try");
            }
            underTest = Collections.checkedList(new ArrayList<>(), String.class);
            test = underTest;
            try {
                test.add("1");
                test.add("2");
                test.add("3");
                test.add(Integer.valueOf(4));
                System.err.println(underTest);
            } catch (ClassCastException ex) { System.err.println("Cast Exceprtion while second try"); }
        }
    }

    public static void main(String[] args) {
        Collect.task01();
    }
}
