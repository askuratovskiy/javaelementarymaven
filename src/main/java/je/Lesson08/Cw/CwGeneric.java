package je.Lesson08.Cw;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CwGeneric {


    static class MyClass{
        private Object val;
        public MyClass(Object arg){
            this.val = val;
        }

        public String toString(){
            return "["+ val +"}";
        }

        public Object getValue(){
            return val;
        }
    }


    public static void main(String[] args) {
        task01();


    }

    public static void task01(){
        List<?> intList = new ArrayList<Integer>();
        List list = new LinkedList();
        list.add("First");
        list.add(10);

        List<String> list2 = list;
        for (Iterator<String> iter = list2.iterator(); iter.hasNext();){
            System.out.printf(iter.next());
        }

    }
}
