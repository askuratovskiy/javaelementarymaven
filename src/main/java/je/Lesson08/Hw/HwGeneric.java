package je.Lesson08.Hw;

import java.util.ArrayList;
import java.util.List;

public class HwGeneric {

    static <T extends Comparable<T>, V extends T> boolean isIn(final T x, final V[ ] y) {
        boolean result = false;
        for (final V value : y) {
            if (x.compareTo(value) == 0) {
                result = true;
                break;
            }
        }
        return result;
    }


    public static Double sum(List<? extends Number> numList) {
        Double result = 0.0;
        for (Number num : numList) {
            result += num.doubleValue();
        }
        return result;
    }

    public static void main(String[] args) {
    }
}
