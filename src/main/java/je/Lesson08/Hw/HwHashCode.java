package je.Lesson08.Hw;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.ArrayList;
import java.util.List;



public class HwHashCode {


    static class Ppp<T1, T2>  {
        T1 fisrt;
        T2 second;

        Ppp(){

        }

        Ppp(T1 one, T2 two){
            this.fisrt = one;
            this.second = two;
        }

        public void setFisrt(T1 fisrt) {
            this.fisrt = fisrt;
        }

        public void setSecond(T2 second) {
            this.second = second;
        }

        public T1 getFisrt() {
            return fisrt;
        }

        public T2 getSecond() {
            return second;
        }
    }

    public static void main(String[] args) {
        Ppp<Integer, Integer> intList = new Ppp<>(1,2);
        Ppp<Integer, Integer> intList1 = new Ppp<>(1,2);

        System.out.println("intList = "+ intList.hashCode());
        System.out.println("intList1 = "+ intList1.hashCode());
//        System.out.printf(intList.hashCode() == intList1.hashCode()? "yes" : "no");
    }

}
