package je.lesson01;

import java.util.Scanner;

public class hwInvitro01 {

    public static void main(String[] args) {
        task01();
        task02();
        task03();
    }

    //Вывод текста на экран
    //“To be or not to be”
    //			    Hamlet
    private static void task01(){
        System.out.println("\"To be or not to be\"\n\t\t\tHamlet");
    }


    //Вывести на экран текст "лесенкой" :
    //Фамилия
    //  Отчество
    //      Имя
    private static void task02(){
        System.out.println("Last Name\n\tSecond Name\n\t\tFirst Name");
    }

    //Discount
    private static void task03(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter sum");
        int sum = scanner.nextInt();
        System.out.println("Enter the procent of discount");
        int discount = scanner.nextInt();
        System.out.println("To pay " + (int)(sum - discount * ((float)sum / 100)));
    }
}
