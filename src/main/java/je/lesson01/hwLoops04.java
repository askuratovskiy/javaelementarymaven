package je.lesson01;

import java.util.Scanner;

public class hwLoops04 {

    public static void main(String[] args) {
        task01();
        task02();
        task03();
        task04();
        task05();
        task06();
        task07();
    }

    //Ввести целое число и найти сумму его цифр.
    //Пример:
    //Введите целое число:
    //1234
    //Сумма цифр числа 1234 равна 10.
    private static void task01() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ввести целое число");
        int num = scanner.nextInt();
        int tmpNum = num;
        int sum = 0;
        while (true) {
            if (tmpNum%10 != 0) {
                sum = sum + tmpNum % 10;
                tmpNum = tmpNum/10;
            } else {
                System.out.println("Сумма цифр числа "+num+ " равна " +sum);
                break;
            }
        }
    }

    //Ввести целое число и определить, верно ли, что в его записи есть две одинаковые цифры.
    //Пример:
    //Введите целое число: Введите целое число:
    //1234 					1224
    //Нет. 					Да.
    private static void task02(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ввести целое число");
        int num = scanner.nextInt();
        String answer = "No";
        while (num != 0) {
            if (num % 10 == (num / 10) % 10) {
                answer = "Yes";
                break;
            } else num = num / 10;
        }
        System.out.println(answer);
    }

    //Задача: Ввести целое положительное число (<2000000) и определить число цифр в нем.
    //Проблема: Как не дать ввести отрицательное число или ноль?
    //Решение: Если вводится неверное число, вернуться назад к вводу данных (цикл!).
    //Особенность: Один раз тело цикла надо сделать в любом случае проверку условия цикла
    // надо делать в конце цикла (цикл с постусловием).
    private static void task03() {
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        int count = 0;
        do{
            System.out.println("Ввести целое положительное число (<2000000)");
            num = scanner.nextInt();
        }while (num <=0 || num >=2000000);

        //Version 1
        System.out.println((int)Math.ceil(Math.log10(num+ 0.5)));

        //Version 2
        String tmpNum = Integer.toString(num);
        System.out.println(tmpNum.length());

        //Version 3
        for ( ; num != 0 ; num /= 10){ ++count; }
        System.out.println(count);

        //Version 4
        while (num != 0){
            count++;
            num /= 10;
        }
        System.out.println(count);



    }

    //Ввести натуральное число и определить, верно ли, что сумма его цифр равна 10.
    //Пример:
    //Введите число >= 0: 						Введите число >= 0:
    //-234 									    1233
    //Нужно положительное число. 				Нет
    //Введите число >= 0:
    //1234
    //Да
    private static int countNumbers(int arg) {
        int sum = 0;
        while (true) {
            if (arg % 10 != 0) {
                sum = sum + arg % 10;
                arg = arg / 10;
            } else  return sum;
        }
    }
    private static void task04(){
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        do{
            System.out.println("Введите число >= 0");
            num = scanner.nextInt();
            if(num <=0) System.out.println("Нужно положительное число.");
        }while (num <=0);
        System.out.println(countNumbers(num) == 10 ? "yes" : "no");
    }

    //Ввести натуральное число и определить, какие цифры встречаются несколько раз.
    //Пример:
    //Введите число >= 0:							Введите число >= 0:
    //2323 										    1234
    //Повторяются: 2, 3 							Нет повторов.
    private static void task05(){
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        do{
            System.out.println("Ввести натуральное число");
            num = scanner.nextInt();
            if(num <=0) System.out.println("Нужно положительное число.");
        }while (num <=0);
        String doubleNums = "";
        for (; num !=0; num /= 10){
            if(num%10 == (num/10)%10){
                doubleNums = doubleNums + num%10 + ", ";
            }
        }
        System.out.println(doubleNums.isEmpty() ?  "Нет повторов." : "Повторяются: " + doubleNums);

    }

    //Пользователь с клавиатуры последовательно вводит целые числа. Как только пользователь
    //ввел 0, необходимо показать на экран сумму всех введенных чисел.
    private static void task06() {
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int num = 0;
        do {
            System.out.println("Ввести натуральное число");
            num = scanner.nextInt();
            if(num >0){ sum = sum + num; }
        } while (num != 0);
        System.out.println("Sum = "+sum);
    }

    //Вывести на экран фигуру (елочка)
    private static void task07(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter num");
        int x = scanner.nextInt();
        for(int i=0; i<x/2; i++){
            for (int b=0; b<x; b++){
                if(b >= (x/2)-i && b <= (x/2)+i) {
                    System.out.print("*");
                }else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }

}
