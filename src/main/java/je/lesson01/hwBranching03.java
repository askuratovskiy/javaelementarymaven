package je.lesson01;

import java.util.Scanner;

public class hwBranching03 {
    public static void main(String[] args) {
        task01();
        task02();
        task03();
        task04();
        task05();
        task06();
        task07();
        task08();
    }

    //Дано двузначное число. Определить:
    //а) какая из его цифр больше: первая или вторая
    //б) одинаковы ли цифры
    private static void task01(){
        System.out.println("Enter a two-digit number");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        if(num/10 > num%10) {System.out.println("The first number is large than the second number");}
        if(num/10 < num%10) {System.out.println("The second number is large than the first number");}
        System.out.println(num/10 == num%10 ? "The numbers are equal" : "The numbers are't equal");
    }

    // Дано натуральное число n.  Определить, является ли год с таким номером високосным.
    //(Год является високосным в двух случаях: либо он кратен 4, но при этом не кратен 100,
    // либо кратен 400. Год не является високосным, если он не кратен 4, либо он кратен 100,
    // но при этом не кратен 400).
    private static void task02(){
        System.out.println("Enter the year");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        System.out.println(num%4==0 && num%100!=0 || num%400==0 ? "This is leap year": "This is't leap year");
    }

    //Определить является ли число a делителем числа b и c одновременно (делится без остатка).
    private static void task03(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter num a");
        int a = scanner.nextInt();
        System.out.println("Enter num b");
        int b = scanner.nextInt();
        System.out.println("Enter num c");
        int c = scanner.nextInt();
        System.out.println(b%a==0 && c%a==0 ? "Yes" : "No");
    }

    //Задача. Фирма набирает сотрудников от 25 до 40 лет включительно.
    //Ввести возраст человека и определить, подходит ли он фирме (вывести ответ «подходит» или «не подходит»).
    private static void task04(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter age");
        int age = scanner.nextInt();
        System.out.println(age >=25 && age<=40 ? "Подходит" : "Не подходит");
    }

    // Ввести номер месяца и вывести название времени года.
    //Пример:
    //Введите номер месяца: 4
    //Время года - весна
    private static void task05(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the month number");
        int num = scanner.nextInt();
        switch (num){
            case 12: case 1: case 2: System.out.println("Winter"); break;
            case 3: case 4: case 5: System.out.println("Spring"); break;
            case 6: case 7: case 8: System.out.println("Summer"); break;
            case 9: case 10: case 11: System.out.println("Autumn"); break;
        default: break;
        }
    }

    //Ввести возраст человека (от 1 до 150 лет) и вывести его вместе с последующим словом «год», «года» или «лет».
    //Пример:
    //Введите возраст: 24
    //Вам 24 года
    //Введите возраст: 57
    //Вам 57 лет
    private static void task06(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите возраст:");
        int num = scanner.nextInt();
        if (num/10==1 && num%10>=1 && num%10<=9 || num%10==0 || num%10>=5 && num%10<=9){
            System.out.println("Вам "+num+" лет");
        }
        if(num/10!=1 && num%10==1){
            System.out.println("Вам "+num+" год");
        }
        if (num/10!=1 && num%10>=2 && num%10<=4){
            System.out.println("Вам "+num+" года");
        }
    }

    //Задача: Ввести номер месяца и вывести количество дней в этом месяце.
    //Решение: Число дней по месяцам:
    //28 дней – 2 (февраль)
    //30 дней – 4 (апрель), 6 (июнь), 9 (сентябрь), 11 (ноябрь)
    //31 день – 1 (январь), 3 (март), 5 (май), 7 (июль), 8 (август), 10 (октябрь), 12 (декабрь)
    //Особенность: Выбор не из двух, а из нескольких вариантов в зависимости от номера месяца.
    private static void task07(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ввести номер месяца");
        int num = scanner.nextInt();
        switch (num){
            case 1: case 3: case 5: case 7: case 8: case 10: case 12: System.out.println("31 день");break;
            case 4: case 6: case 9: case 11: System.out.println("30 дней");break;
            case 2: System.out.println("28 дней");break;
        default: break;
        }


    }

    //Создать калькулятор:
    // Пользователь вводит 1-ое число, потом 2-ое число, а потом операцию, которую хочет выполнить (+ - * / ).
    //Калькулятор выдает результат.
    private static void task08(){
        Scanner scanner = new Scanner(System.in);
        int result=0;
        boolean loop = true;
        while (true) {
            System.out.println("Введите 1-ое число");
            int firstNum = scanner.nextInt();
            System.out.println("Введите 2-ое число");
            int secondNum = scanner.nextInt();
            System.out.println("Что нужно сделать (+ - * / )");
            char act = scanner.next().charAt(0);
            switch (act) {
                case '+': result = firstNum + secondNum; break;
                case '-': result = firstNum - secondNum; break;
                case '*': result = firstNum * secondNum; break;
                case '/': result = firstNum / secondNum; break;
            default: break;
            }
            System.out.println("Результат = "+result);
            System.out.println("Очистить результат? y/n");
            if (scanner.next().charAt(0) == 'y') { result = 0;}
            System.out.println("Завершить калькулятор? y/n");
            if (scanner.next().charAt(0) == 'y') { loop = false; break; }
        }


    }

}
