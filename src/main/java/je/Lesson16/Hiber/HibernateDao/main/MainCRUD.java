package je.Lesson16.Hiber.HibernateDao.main;

import je.Lesson16.Hiber.HibernateDao.model.Book;
import je.Lesson16.Hiber.HibernateDao.model.Users;
import je.Lesson16.Hiber.HibernateDao.model.Stock;
import je.Lesson16.Hiber.HibernateDao.service.DaoService;

import javax.persistence.NoResultException;

import static java.lang.System.identityHashCode;
import static java.lang.System.out;
import static java.lang.System.setOut;

import java.lang.reflect.Field;
import java.util.List;

public class MainCRUD {
    public static void main(String[] args) {
        Book book1 = new Book("new_persist", "author_persist");
        Book book2 = new Book("title2", "author2");
        DaoService<Book> dao = new DaoService<>();
        try {
            /* Persist  - has already working*/
            //dao.persist(book1);

            /*FindById -  */
            //Object update = dao.findById("7");

            /*Update */
//            Object findById  = dao.findById("7");
//            Book bookUpdate = new Book();
//            Class findByIdClass = findById.getClass();
//            Class bookUpdateClass = bookUpdate.getClass();
//            Field [] findByIdFields = findByIdClass.getDeclaredFields();
//            Field [] bookUpdateFields = bookUpdateClass.getDeclaredFields();
//            for (Field findByIdField : findByIdFields){
//                for (Field bookUpdateField : bookUpdateFields){
//                    if(findByIdField.getName().equals(bookUpdateField.getName())){
//                        bookUpdateField.setAccessible(true);
//                        findByIdField.setAccessible(true);
//                        bookUpdateField.set(bookUpdate, findByIdField.get(findById));
//                    }
//                }
//            }
//            bookUpdate.setTitle("new_persist");
//            dao.update(bookUpdate);
//            System.out.println(bookUpdate.toString());

            /*Delete */
//            dao.delete(book1);


            /*selectByValueToRow*/
//            Object book = dao.selectByValueToRow(new Book(), "title", "title");
//            System.out.println(book.toString());
//            Object stock = dao.selectByValueToRow(new Stock(), "STOCK_CODE", "111");
//            System.out.println(stock.toString());
//            System.exit(1);

            /*
            *
            * Users test CRUD
            *
            * */

            //Object book = dao.selectByValueToRow(new Book(), "author", "Wqer");
            //System.out.println(book.toString()+" Main");
//            Users user1 = new Users("Vasya5", "vasya1", "Popov", "Vasya", "Andeevich", 25, "man");
//            dao.persist(user1);

            List selectUser = dao.selectByValueToRowAll(new Users(), "sex", "man");
            System.out.println(selectUser.toString()+" Main");
//
//            Object selectUserLimit = dao.selectByValueToRow(new Users(), "l_name", "Vasya4", 1);
//            System.out.println(selectUserLimit.toString()+" Main");
//            Object findById = dao.findById(new Users(), 2);
//            System.out.println(findById.toString());

            System.exit(1);

            }
            catch(NoResultException e){
                System.out.println("NoResultException = " + e);
                System.exit(1);
            }
            catch(IndexOutOfBoundsException e){
                System.out.println("IndexOutOfBoundsException = " + e);
                System.exit(1);
            }
            catch(Exception e){
                System.out.println("Exception = " + e);
                System.exit(1);
            }

        }

    }

