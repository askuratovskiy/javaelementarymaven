package je.Lesson16.Hiber.HibernateDao.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import je.Lesson16.Hiber.HibernateDao.model.Book;
import je.Lesson16.Hiber.HibernateDao.model.Users;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ObjectDao<T> implements ObjectDaoInterface<Object> {

    private static final  SessionFactory sessionFactory;
    private static Session currentSession;
    private Transaction currentTransaction;

    public ObjectDao() {
    }

    static {
        try {
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml")
                    .build();
            Metadata metaData = new MetadataSources(standardRegistry)
                    .getMetadataBuilder()
                    .build();
            sessionFactory = metaData.getSessionFactoryBuilder()
                    .build();
        } catch (Throwable th) {
            System.err.println("Enitial SessionFactory creation failed" + th);
            throw new ExceptionInInitializerError(th);
        }
    }

    public Session openCurrentSession(){
        currentSession = sessionFactory.openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction(){
        currentSession = sessionFactory.openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public Session getSession(){
        return currentSession;
    }

    @Override
    public void persist(Object entity) {
        getSession().save(entity);
    }

    @Override
    public void update(Object entity) {
        getSession().update(entity);
    }

    @Override
    public Object findById(Object entity, int id) {
        String tableName = entity.getClass().getSimpleName();
        Query query = getSession()
                .createQuery("from "+tableName+" where id = "+id);
        Object object = query.getSingleResult();
        return object;
    }
    @Override
    public List<?> selectByValueToRowAll(Object entity, String field, String value) {
        String tableName = entity.getClass().getSimpleName();
        Query query = getSession()
                .createQuery("from "+tableName+" where "+field+" = :value")
                .setParameter("value", value);
        Object o = query.scroll();
        List<?> list = query.list();
        return list;
    }
    public Object selectByValueToRow(Object entity, String field, String value, int limit) {
        String tableName = entity.getClass().getSimpleName();
        Query query = getSession()
                .createQuery("from "+tableName+" where "+field+" = :value")
                .setParameter("value", value);
        query.setMaxResults(limit);
        Object tmp = query.getSingleResult();
        return tmp;
    }

    @Override
    public void deleteById(Object entity) {
        getSession().delete(entity);
    }

    @Override
    public void deleteAll(Object entity) {
        String tableName = entity.getClass().getSimpleName();
        Query query = getSession().createQuery("from "+tableName+" where id > 0");
    }

    @Override
    public void deleteToValue(Object entity, String entityName) {
        getSession().delete(entityName, entity);
    }

    @Override
    public List<Object> findAll() {
        return null;
    }

    @Override
    public void deleteAll() {

    }

}
