package je.Lesson16.Hiber.HibernateDao.dao;

import java.io.Serializable;
import java.util.List;

public interface ObjectDaoInterface<T> {

    // +
    public void persist(T entity);

    // +
    public void update(T entity);

    public void deleteById(T entity);

    public void deleteAll(T entity);

    // +
    public T findById(T entity, int id);

    // +
    public T selectByValueToRowAll(Object name, String table, String value);

    // +
    public T selectByValueToRow(Object name, String table, String value, int limit);

    public void deleteToValue(T entity, String entityName);

    public List<T> findAll();

    public void deleteAll();

}
