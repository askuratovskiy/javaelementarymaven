package je.Lesson16.Hiber.HibernateDao.service;


import je.Lesson16.Hiber.HibernateDao.dao.ObjectDao;
import je.Lesson16.Hiber.HibernateDao.model.Book;
import je.Lesson16.Hiber.HibernateDao.model.Users;

import java.util.List;


public class DaoService<T> {

    private static ObjectDao objectDao;

    public DaoService() {
        objectDao = new ObjectDao();
    }

    public void persist(Object entity) {
        objectDao.openCurrentSessionWithTransaction();
        objectDao.persist(entity);
        objectDao.closeCurrentSessionWithTransaction();
    }

    public void update(Object entity) {
        objectDao.openCurrentSessionWithTransaction();
        objectDao.update(entity);
        objectDao.closeCurrentSessionWithTransaction();
    }

    public void deleteById(Object entity){
        objectDao.openCurrentSessionWithTransaction();
        objectDao.deleteById(entity);
        objectDao.closeCurrentSessionWithTransaction();
    }

    public void deleteAll(Object entity){
        objectDao.openCurrentSessionWithTransaction();
        objectDao.deleteById(entity);
        objectDao.closeCurrentSessionWithTransaction();
    }

    public void deleteToValue(Object entity, String entityName){
        objectDao.openCurrentSessionWithTransaction();
        objectDao.deleteToValue(entity, entityName);
        objectDao.closeCurrentSessionWithTransaction();
    }

    public Object findById(Object entity, int id) {
        objectDao.openCurrentSession();
        Object object = objectDao.findById(entity, id);
        objectDao.closeCurrentSession();
        return object;
    }

    public List<?> selectByValueToRowAll(Object entity, String field, String value){
        objectDao.openCurrentSessionWithTransaction();
        List<?> list = objectDao.selectByValueToRowAll(entity, field, value);
        objectDao.closeCurrentSessionWithTransaction();
        return list;
    }
    public Object selectByValueToRow(Object entity, String field, String value, int limit){
        objectDao.openCurrentSessionWithTransaction();
        Object object = objectDao.selectByValueToRow(entity, field, value, limit);
        objectDao.closeCurrentSessionWithTransaction();
        return object;
    }

    public ObjectDao ObjectDao() {
        return objectDao;
    }
}
