package je.Lesson16.Hiber.HibernateDao.model;

import javax.persistence.*;

@Entity
@Table(name="stock")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STOCK_ID")
    private int STOCK_ID;

    @Column(name="STOCK_CODE")
    private String STOCK_CODE;

    @Column(name="STOCK_NAME")
    private String STOCK_NAME;

    public Stock (){ }

    public Stock (int STOCK_ID, String STOCK_CODE, String STOCK_NAME){
        STOCK_ID = this.STOCK_ID;
        STOCK_CODE = this.STOCK_CODE;
        STOCK_NAME = this.STOCK_NAME;
    }

    public int getSTOCK_ID() {
        return STOCK_ID;
    }

    public String getSTOCK_CODE() {
        return STOCK_CODE;
    }
    public void setSTOCK_CODE(String code) {
        this.STOCK_CODE = code;
    }

    public String getSTOCK_NAME() {
        return STOCK_NAME;
    }
    public void setSTOCK_NAME(String name) {
        this.STOCK_NAME = name;
    }



}
