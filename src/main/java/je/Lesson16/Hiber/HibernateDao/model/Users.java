package je.Lesson16.Hiber.HibernateDao.model;

import javax.persistence.*;
import java.util.Iterator;

@Entity
@Table(name = "users")
public final class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private String id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "f_name")
    private String fName;

    @Column(name = "l_name")
    private String lName;

    @Column(name = "m_name")
    private String mName;

    @Column(name = "age")
    private int age;

    @Column(name = "sex")
    private String sex;

    public Users(){
    }

    public Users(String id, String login, String password, String fName, String lName, String mName, int age, String sex){
        this.id = id;
        this.login = login;
        this.password = password;
        this.fName = fName;
        this.lName = lName;
        this.mName = mName;
        this.age = age;
        this.sex = sex;
    }

    public Users(String login, String password, String fName, String lName, String mName, int age, String sex){
        this.login = login;
        this.password = password;
        this.fName = fName;
        this.lName = lName;
        this.mName = mName;
        this.age = age;
        this.sex = sex;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getLogin(){ return login; }
    public void setLogin(String login) { this.login = login; }

    public String getPassword(){ return password; }
    public void setPassword(String password){ this.password = password; }

    public String getFName(){ return fName; }
    public void setFName(String fName) { this.fName = fName; }

    public String getLname(){ return lName; }
    public void setLname(String lName){ this.lName = lName; }

    public String getMname(){ return mName; }
    public void setMname(String mName) { this.mName = mName; }

    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }

    public String getSex(){ return sex; }
    public void setSex(String sex) { this.sex = sex; }

    @Override
    public String toString() {
        return "User: " +
                this.id + ", " +
                this.login + ", " +
                this.password + ", " +
                this.fName + ", " +
                this.lName + ", " +
                this.mName + ", " +
                this.age + ", " +
                this.sex;
    }
}
