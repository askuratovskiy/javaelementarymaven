package je.lesson05.HwCalc;


import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class HwLambdaJava8 {
    public static void main(String[] args) {
        compaTo.test02();
    }



    private static class compaTo {
        static void test01() {
        List<String> names = Arrays.asList("peter", "annatt", "mike", "xenia");
        Collections.sort(names,new Comparator<String>(){
            @Override
            public int compare (String o1, String o2){
            return o1.compareTo(o2);
        }});
        Collections.sort(names,(a,b)-> { return a.compareTo(b);});
        System.out.println(names);
        }

        static void test02(){
            Predicate<String> predicate = s -> s.length() > 0;
            System.out.println(predicate.test("qwe"));
            Predicate<String>isEmpty = s -> s.isEmpty();
            System.out.println(isEmpty.test(""));
            Function<String,Integer> toInteger = Integer::valueOf;
            System.out.println(toInteger.apply("1234"));
        }
    }
}
