package je.lesson05.HwCalc;

import java.util.Arrays;
import java.util.Scanner;

public class HwStringBuffer17 {
    public static void main(String[] args) {
//        task01();
//        task02();
        task03();
    }

    private static void task01(){
        String sentence01 = "WooHoo";
        String sentence02 = "HelloThere";
        String sentence03 = "abcdef";
        System.out.println(sentence01.substring(0,sentence01.length()/2));
        System.out.println(sentence02.substring(0,sentence02.length()/2));
        System.out.println(sentence03.substring(0,sentence03.length()/2));
    }
    private static String getCharsTask01(String str){
        char [] tmp = new char[str.length()/2];
        str.getChars(0, str.length()/2, tmp, 0);
        return String.valueOf(tmp);
    }

    private static void task02(){
        String str = "ly";
        System.out.println(str.startsWith("ly")?"yes":"no");
    }

    private static void task03(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a sentence:\t");
        String text = scanner.nextLine();
        String[] words = text.split("\\W+");
        for(int i=words.length-1; i>=0; i--){
            for (int b=0; b<i; b++){
                if(words[b].length()>words[i].length()){
                    String tmp = words[b];
                    words[b] = words[i];
                    words[i] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(words));
    }


}
