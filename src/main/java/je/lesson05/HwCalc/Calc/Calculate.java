package je.lesson05.HwCalc.Calc;

@FunctionalInterface
interface Calculate<T extends Number> {
    public T exec(T num1, T num2);
}

