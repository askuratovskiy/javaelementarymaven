package je.lesson05.HwCalc.Calc;

import je.lesson05.HwCalc.Calc.Calculate;

import java.util.*;

public class Main {
    static class HashtableInterfaceCalculator {
        Map<String, Calculate<Double>> hashMap = new HashMap<>();
        HashtableInterfaceCalculator(){
            hashMap.put("+", (num1, num2) -> {return num1 + num2;});
            hashMap.put("-", (num1, num2) -> {return num1 - num2;});
            hashMap.put("*", (num1, num2) -> {return num1 * num2;});
            hashMap.put("/", (num1, num2) -> {return num1 / num2;});
        }
    }
    public static void main(String[] args)  {
        Map<String, Calculate<Double>> interCalcHashMap = new HashtableInterfaceCalculator().hashMap;
        Scanner scanner = new Scanner(System.in);
        boolean end = false;
        do{
            System.out.println("Enter first num");
            Double d1 = scanner.nextDouble();
            System.out.println("Enter first operation");
            String symbol = scanner.next();
            System.out.println("Enter second num");
            Double d2 = scanner.nextDouble();
            Calculate<Double> calculate = interCalcHashMap.get(symbol);
            System.out.println("Result = " + calculate.exec(d1, d2));
            System.out.println("Do you want continued y/n");
            if(scanner.next().equalsIgnoreCase("n")){
                end = true;
                System.out.println("Bye");
            }
        }while (end==false);
    }
}







