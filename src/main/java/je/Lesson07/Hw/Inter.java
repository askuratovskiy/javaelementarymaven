package je.Lesson07.Hw;
@FunctionalInterface
public interface Inter<T extends Number> {

    public T exec(T var1);

}
