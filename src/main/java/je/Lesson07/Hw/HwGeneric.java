package je.Lesson07.Hw;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HwGeneric {

    public static Double sum(List<? extends Number> numList){
        Double result = 0.0;
        for (Number num : numList){
            result += num.doubleValue();
        }
        return result;
    }

    public static <T> void genArray (List<T> listm, T val, int size){
        for (int i = 0; i<size; i++){
            listm.add(val);
        }
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        genArray(list, 1, 10);
        System.out.println(list);

        List<String> strList = new ArrayList<>();
        genArray(strList, "fill", 30);
        System.out.println(strList);





    }

}
