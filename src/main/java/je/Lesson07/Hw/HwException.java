package je.Lesson07.Hw;

import java.util.Scanner;

public class HwException {

    static class UncheckerException extends RuntimeException{

        private String str;
        UncheckerException(String  arg){
            this.str = arg;
        }

        public String toString(){
            return this.str;
        }
    }

    static class CheckedException extends Exception{
        private int detail;
        CheckedException(int a) {
            this.detail = a;
        }
        public String toString(){
            return "My exception "+ detail;
        }

    }

    public static void computer(int a) throws CheckedException{
        System.out.println("called comp "+a);
        if (a > 10) {
            throw new CheckedException(a);
        }
        System.out.println("Normal exit");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                computer(scanner.nextInt());
                computer(scanner.nextInt());
            } catch (CheckedException e) {
                System.out.println("sdsd " + e);
            }catch (UncheckerException e){
                System.out.println("aaaaa" + e);
               // return "222";
            }finally {
                System.exit(0);
             //   return "111";
            }
        }

    }

}



