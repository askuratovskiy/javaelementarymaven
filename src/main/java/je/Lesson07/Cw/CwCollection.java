package je.Lesson07.Cw;

import java.util.ArrayList;
import java.util.Iterator;

public class CwCollection {

    public static void main(String[] args) {
        System.out.println(task01());

    }

    private static String task01(){
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i<100; i++){
            arrayList.add(i);
        }

        String res = "";
        Iterator iter = arrayList.iterator();

        for (int i = 0; iter.hasNext(); i++){
            if(i%6==0){res +="\n";}
            res += " "+iter.next().toString();
        }
        return res;
    }


}
