package je.Lesson09.Hw;


public class OverrideArrayList<T>{

    private final int initSize = 10;
    private Object [] list;
    private int currentSize = 0;

    public OverrideArrayList(){
        this.list = new Object[initSize];
    }

    public OverrideArrayList(int initCapacity){
        this.list = new Object[initCapacity];
    }

    public void addNext(T item){
        if(currentSize == list.length){
            resizeUp();
        }
        list[currentSize] = item;
        currentSize++;
    }

    public void replaceToIndex(int index, T item){
        if(list.length-1 >= index)
            list[index] = item;
    }

    public void insertToIndex(int index, T item){
        if(list.length > currentSize && list.length > index){
            for(int i = currentSize-1; i>=index; i--){
                list[i+1] = list[i];
            }
            list[index] = item;
            currentSize++;
        }else {
            resizeUp();
            for(int i = currentSize-1; i>=index; i--){
                list[i+1] = list[i];
            }
            list[index] = item;
            currentSize++;
        }
    }

    public void removeLast(){
        list[currentSize-1] = null;
        currentSize--;
    }

    public T getToIndex(int index){ return (T) list[index]; }

    public String getToValues(T value){
        String tmp = "";
        for (int i =0; i<currentSize; i++){
            if(list[i] == value ){
                tmp = tmp+i+",";
            }
        }
        return tmp;
    }

    public void trim(){ resizeDown(); }

    public int arrLenght(){
        return list.length;
    }

    public int arrSize() {
        return currentSize;
    }

    private void resizeUp(){copyResize(list.length*2);}

    private void resizeDown(){copyResize(list.length/2);}

    private void copyResize(int newSize){
        Object [] tmp = new Object[newSize];
        for(int i=0; i<currentSize; i++){
            tmp[i] = list[i];
        }
        list = tmp;
    }

}
