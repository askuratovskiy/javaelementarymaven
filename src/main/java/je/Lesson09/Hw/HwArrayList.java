package je.Lesson09.Hw;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class HwArrayList {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>(10);
        for (int i = 0; i <10; i++ ){
            list.add(i);
        }
        for (Integer i : list){
            System.out.print(i+", ");
        }

    }

}
