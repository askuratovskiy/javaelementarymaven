package je.Lesson09.Cw;

public class ChickenVoice{
    static EggVoice egg;//Побочный поток
    public static void main(String[] args){
        egg = new EggVoice();//Создание потока
        System.out.println("Спор начат...");
        egg.start();//Запуск потока
        for(int i = 0; i < 5; i++){
            try{
                Thread.sleep(1000);//Приостанавливает поток на 1 секунду
            }catch(InterruptedException e){}
            System.out.println("курица!");
        }
        if(egg.isAlive()){//Если оппонент еще не сказал последнее слово
            try{
                egg.join();//Подождать пока оппонент закончит высказываться.
            }catch(InterruptedException e){}
            System.out.println("Первым появилось яйцо!");
        } else {//если оппонент уже закончил высказываться
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}
