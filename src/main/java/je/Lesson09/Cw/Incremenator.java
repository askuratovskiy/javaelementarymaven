package je.Lesson09.Cw;

class Incremenator extends Thread{
    private volatile boolean mIsIncrement = true;
    public void changeAction(){//Меняет действие на противоположное
        mIsIncrement = !mIsIncrement;
    }
    @Override public void run(){
        do {
            if(!Thread.interrupted()){//Проверка прерывания
                if(mIsIncrement) Program.mValue++;
                else Program.mValue--;
                System.out.print(Program.mValue + " ");//Вывод текущего значения переменной
            } else {
                System.out.printf("first");
                return;
            }//Завершение потока
            try {
//Приостановка потока на 1 сек.
                Thread.sleep(1000);
            } catch(InterruptedException e){
                System.out.printf("second");
                return;//Завершение потока после прерывания
            }
        } while(true);
    }
}
