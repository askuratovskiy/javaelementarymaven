package je.Lesson24.AbstractFactory;

public class TeddyToysFactory implements ToyFactory {
    public Bear getBear() {
        return new TeddyBear();
    }
    public Cat getCat() {
        return new TeddyCat();
    }
}
