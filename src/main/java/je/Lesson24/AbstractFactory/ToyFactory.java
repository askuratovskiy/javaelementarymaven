package je.Lesson24.AbstractFactory;

public interface ToyFactory {
    Bear getBear();
    Cat getCat();
}
