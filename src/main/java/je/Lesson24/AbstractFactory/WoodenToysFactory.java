package je.Lesson24.AbstractFactory;

public class WoodenToysFactory implements ToyFactory {
    public Bear getBear() {
        return new WoodenBear();
    }
    public Cat getCat() {
        return new WoodenCat();
    }
}

