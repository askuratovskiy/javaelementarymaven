package je.Lesson24.AbstractFactory;

public class WoodenBear extends Bear {
    private String name;

    @Override
    public String getName() {
        return "Wooden Bear";
    }
}
