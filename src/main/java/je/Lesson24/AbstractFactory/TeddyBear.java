package je.Lesson24.AbstractFactory;

public class TeddyBear extends Bear {
    private String name;

    @Override
    public String getName() {
        return "Teddy Bear";
    }
}
