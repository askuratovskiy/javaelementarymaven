package je.Lesson24.AbstractFactory;

public class WoodenCat extends Cat {
    private String name;

    @Override
    public String getName() {
        return "Wooden Cat";
    }
}
