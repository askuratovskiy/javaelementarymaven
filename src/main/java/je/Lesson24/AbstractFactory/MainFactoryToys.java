package je.Lesson24.AbstractFactory;

public class MainFactoryToys {

    public static void main(String[] args) {
        ToyFactory toyFactory = new WoodenToysFactory();
        Bear bear = toyFactory.getBear();
        Cat cat = toyFactory.getCat();
        System.out.printf("I've got %s and %s", bear.getName(), cat.getName());
        System.out.println();


        toyFactory = new TeddyToysFactory();
        Bear bear1 = toyFactory.getBear();
        Cat cat1 = toyFactory.getCat();
        System.out.printf("I've got %s and %s", bear1.getName(), cat1.getName());

    }

}
