package je.Lesson24.AbstractFactory;

public class TeddyCat extends Cat {
    private String name;

    @Override
    public String getName() {
        return "Teddy Cat";
    }
}
