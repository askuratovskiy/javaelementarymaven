package je.Lesson06.Hw;

import java.util.HashSet;
import java.util.Set;

public class HwEnum15 {

    static class CarType {
        enum Sedan {
            Lada("Granta", 1.6f), Volvo("V60", 2), Ranult("Logan", 1.3f), Lifan("Solano", 1.7f);

            private String model;
            private float engineCapacity;
            Sedan(String model) { this.model = model; }
            Sedan(String model, float engineCapacity) { this.model = model; this.engineCapacity = engineCapacity; }
            String getModel() { return this.model; }
            float getEngineCapacity() { return this.engineCapacity; }
        }

        enum Limousine {
            ЗИЛ("ЗИЛ-41047", 3f), Mercedes_Benz("V60", 2), Ranult("W100", 4.2f), Lincoln("Town Car", 2.7f);

            private String model;
            private float engineCapacity;
            Limousine(String model) { this.model = model; }
            Limousine(String model, float engineAmount) { this.model = model; this.engineCapacity = engineAmount; }
            String getModel() { return this.model; }
            float getEngineCapacity() { return this.engineCapacity; }
        }

        enum SportCar {
            Mazda, Mclaren("570GT", 2), Bugatti("Vision Gran Turismo", 4.2f), Acura("NSX", 2.7f);

            private String model;
            private float engineCapacity;
            SportCar() { this.model = null; this.engineCapacity = 0;}
            SportCar(String model) { this.model = model; }
            SportCar(String model, float engineAmount) { this.model = model; this.engineCapacity = engineAmount; }
            String getModel() { return this.model; }
            float getEngineCapacity() { return this.engineCapacity; }
        }

        enum CrossOver {
            Renault("Duster",4), Chevrolet("Niva", 2), Nissan("Terrano", 4.2f);

            private String model;
            private float engineCapacity;
            CrossOver() { this.model = null; this.engineCapacity = 0;}
            CrossOver(String model) { this.model = model; }
            CrossOver(String model, float engineAmount) { this.model = model; this.engineCapacity = engineAmount; }
            String getModel() { return this.model; }
            float getEngineCapacity() { return this.engineCapacity; }
        }

        enum HatchBack {
            Skoda("Fabia",4), KIA("Ceed", 2), Renault("Sandero", 4.2f);

            private String model;
            private float engineCapacity;
            HatchBack() { this.model = null; this.engineCapacity = 0;}
            HatchBack(String model) { this.model = model; }
            HatchBack(String model, float engineAmount) { this.model = model; this.engineCapacity = engineAmount; }
            String getModel() { return this.model; }
            float getEngineCapacity() { return this.engineCapacity; }
        }
    }

    public static void main(String[] args) {

        Set<Enum> typeList = new HashSet();
//        typeList.add(CarType);
//        typeList.add("Limousine");
        System.out.println(typeList.size());
        String renault = CarType.HatchBack.Renault.toString();


    }
}

