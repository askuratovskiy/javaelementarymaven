package je.Lesson15;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class HwJDBC {

    private static Connection con = null;
    private static String username = "root";
    private static String password = "empty";
    private static String URL = "jdbc:mysql://localhost:3306/hillel";

    public static void printAllRows(ResultSet rs) throws SQLException {
        int countColumns = rs.getMetaData().getColumnCount();
        while (rs.next()) {
            for (int i = 1; i <= countColumns; i++) {
                System.out.print(rs.getString(i) + "\t");
            } System.out.println();
        } System.out.println();
    }

    public static void main(String[] args) throws SQLException {
        con = DriverManager.getConnection(URL, username, password);
        if (con != null) System.out.println("Connection Successful !\n");
        if (con == null) System.exit(0);
        Statement st = con.createStatement();
        ResultSet rsStart = st.executeQuery("select * from je_db");
        printAllRows(rsStart);
        st.executeUpdate("update je_db set fName='Sergey' where lName='Varenik'");
        ResultSet rsEnd = st.executeQuery("select * from je_db");
        printAllRows(rsEnd);
        st.executeUpdate("update je_db set fName='Zhenya' where lName='Varenik'");
        if (rsStart != null) rsStart.close();
        if (rsEnd != null) rsEnd.close();
        if (st != null) st.close();
    }
}

