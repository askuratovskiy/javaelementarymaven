package je.lesson04;

public class cw01 {

    public static void main(String[] args) {
        g1();

    }
    public static class Outer {
        int x = 0;
        class Inner1 {
            public int f() {
                return ++x;
            }
        }
        public void g(final int j) {
            final int k = j;
            class Inner2 {
                public int f(int r) {
                    return r * k + j;   // Здесь и j и k должны быть final
                }
            }
            Inner2 i1 = new Inner2();
            int s = i1.f(10);
            System.out.println(s);
        }
    }
    public static void g1() {
        Outer t1 = new Outer();
        Outer.Inner1 i2 = t1.new Inner1();
        System.out.println(i2.f());
        t1.g(5);
    }



}
