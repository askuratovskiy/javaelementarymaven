package je.lesson04;

import java.util.function.*;
@FunctionalInterface
public interface Shape {
    void draw();

}
