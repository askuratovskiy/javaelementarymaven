package je.Human;

public abstract class Human<T1, T2, T3, T4, T5> {
    private T1 fname;
    private T2 sName;
    private T3 lName;
    private T4 sex;
    private T5 age;
    Human(T1 fName, T2 sName, T3 lName, T4 sex, T5 age){
        this.fname = fName;
        this.sName = sName;
        this.lName = lName;
        this.sex = sex;
        this.age = age;
    }

//    public String toString() {
//        return fname +" "+ sName +" "+ lName + " " + sex +" "+ age;
//    }


}
